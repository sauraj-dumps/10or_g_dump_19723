#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:25689388:e6cf5b3442c28fc5ae3ce86e81e60e25622d1fdc; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:23686440:f0b5dc0139b6b38a4fe81d61f86c6b1ca8406a7f EMMC:/dev/block/bootdevice/by-name/recovery e6cf5b3442c28fc5ae3ce86e81e60e25622d1fdc 25689388 f0b5dc0139b6b38a4fe81d61f86c6b1ca8406a7f:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
